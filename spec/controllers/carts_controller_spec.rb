require 'rails_helper'
require 'database_cleaner_helper'

RSpec.describe CartsController, :type => :controller do
  fixtures :carts
  fixtures :line_items
  fixtures :products

  let(:one_item_cart) { carts(:one_item_cart)}
  let(:two_item_cart) { carts(:two_item_cart)}

  describe "GET index" do
    context "accessing an invalid cart" do
      it "redirects to the store url" do
        get :show, id: 10
        expect(response).to redirect_to store_url
      end

      it "sets a notice about the invalid cart" do
        get :show, id: 10
        expect(flash[:notice]).to eql('Invalid cart')
      end
    end

    context "try to access someone elses cart" do
      it "redirect to the store url" do
        session[:cart_id] = one_item_cart.id
        get :show, id: two_item_cart.id

        expect(response).to redirect_to(store_url)
      end

      it "should show an error" do
        session[:cart_id] = one_item_cart.id
        get :show, id: two_item_cart.id

        expect(flash[:notice]).to eql "Invalid cart"
      end
    end

    context "access a valid cart" do
      before :each do
        session[:cart_id] = one_item_cart.id
      end

      it "renders show template" do
        get :show, id: one_item_cart.id
        expect(response).to render_template(:show)
      end

      it "assigns the cart" do
        get :show, id: one_item_cart.id
        expect(assigns(:cart)).to eql one_item_cart 
      end
    end
  end

  describe "PUT update" do
    let(:line1) { two_item_cart.line_items.first }
    let(:line2) { two_item_cart.line_items.last }

    def cart_params(line_attrs)
      {
        id: two_item_cart.id,
        cart: {
          line_items_attributes: line_attrs
        }
      }
    end

    it "updates the line item quantities" do
      line_items_attributes = {
        "0": {id: line1.id, quantity: 5},
        "1": {id: line2.id, quantity: 7}
      }

      put :update, cart_params(line_items_attributes)

      expect { line1.reload }.to change { line1.quantity }
      expect { line2.reload }.to change { line2.quantity }
    end

    it "doesn't allow a change to product id" do
      line_items_attributes = {
        "0": {id: line1.id, product_id: 100},
        "1": {id: line2.id, product_id: 101}
      }

      put :update, cart_params(line_items_attributes)

      expect { line1.reload }.to_not change { line1.product_id }
      expect { line2.reload }.to_not change { line2.product_id }
    end
  end

  describe "DELETE destroy" do
    it 'destroys owners cart"' do
      expect do
        session[:cart_id] = one_item_cart.id
        delete :destroy, id: one_item_cart.id
      end.to change { Cart.count }.by(-1)
    end

    it 'leaves owners cart if id is not a match' do
      expect do
        session[:cart_id] = one_item_cart.id
        delete :destroy, id: two_item_cart.id
      end.not_to change { Cart.count }
    end
  end
end
