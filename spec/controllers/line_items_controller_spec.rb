require 'rails_helper'
require 'database_cleaner_helper'

RSpec.describe LineItemsController, :type => :controller do

  describe "POST #create" do
    fixtures :carts
    fixtures :products

    let(:cart) { carts(:one_item_cart) }

    it "redirects to the store index" do
      expect { post :create, product_id: products(:coffeescript).id }.to change { LineItem.count }.by(1)

      expect(response).to redirect_to(store_path)
    end

    it "ups the quanitity for a line item" do
      session[:cart_id] = cart.id

      item = cart.line_items.first
      old_quantity= item.quantity

      post :create, product_id: item.product.id
      item.reload
      expect(item.quantity).to eql(old_quantity + 1)
    end

    it 'resets the store counter' do
      post :create, product_id: products(:coffeescript).id
      expect(controller.store_counter).to eql 1
    end
  end

  describe "DELETE #destroy" do
    fixtures :carts

    let(:my_cart) { carts(:one_item_cart) }

    before :each do
      allow(Cart).to receive(:find).and_return my_cart
      allow(LineItem).to receive(:find).and_return double
    end

    it "redirects to the cart index" do
      session[:cart_id] = my_cart.id
      allow(my_cart).to receive(:remove).and_return true

      post :destroy, id: 100

      expect(response).to redirect_to(cart_path(my_cart))
    end

    it "adds a flash message when item can't be removed" do
      my_cart.errors.add(:base, "Error")
      # Error happened trying to remove item
      allow(my_cart).to receive(:remove).and_return false 

      post :destroy, id: 100

      expect(flash[:errors]).to eql ["Error"]
    end
  end
end
