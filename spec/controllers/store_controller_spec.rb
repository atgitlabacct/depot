require 'rails_helper'
require 'database_cleaner_helper'

RSpec.describe StoreController, :type => :controller do

  let(:product_valid_attributes) { 
    {title: "some title that works", 
     description: "some other description", price: 10.10 }
    }

  describe "GET index" do
    it "returns http success" do
      get :index
      expect(response).to be_success
    end

    it "counts the times index is accessed" do
      get :index
      get :index
      expect(controller.store_counter).to eql 2
    end

    it "assigns the products" do
      Product.create! product_valid_attributes
      get :index
      expect(assigns(:products)).to_not be_empty
    end

    it "renders the template" do
      get :index
      expect(response).to render_template("index")
    end
  end

end
