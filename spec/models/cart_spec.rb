require 'rails_helper'
require 'database_cleaner_helper'

RSpec.describe Cart, :type => :model do

  fixtures :products
  fixtures :line_items
  fixtures :carts

  describe "#add_product" do
    let(:coffeescript) { products(:coffeescript) }
    let(:ruby) { products(:ruby) }
    let(:cart) { Cart.new }

    it 'adds line items to the cart' do
      item1 = cart.add_product(coffeescript)
      item2 = cart.add_product(ruby)
      expect(cart.line_items).to match_array([item1, item2])
    end

    it "increases the quantity if same product is added" do
      cart.add_product(coffeescript)
      line_item = cart.add_product(coffeescript)
      expect(line_item.quantity).to eql 2
    end

    it 'sets the price in for the line item' do
      line_item = cart.add_product(coffeescript)
      expect(line_item.price).to eql(coffeescript.price)
    end
  end

  describe "#remove" do
    let(:cart) { carts(:one_item_cart) }
    let(:line) { cart.line_items.first }

    context "item exists in the cart" do

      it "removes the line item" do
        expect(cart.remove(line)).to eql true
        expect(cart.line_items).to_not include(line)
      end

      it "doesn't set any errors" do
        cart.remove(line)
        expect(cart.errors[:base]).to be_empty
      end
    end

    context "item is not part of the cart" do
      let(:item_in_diff_cart) { carts(:two_item_cart).line_items.first }

      it "doesn't remove any lines" do
        orig_lines = cart.line_items

        expect(cart.remove(item_in_diff_cart)).to eql false
        orig_lines.each do |line|
          expect(cart.line_items).to include(line)
        end
      end

      it "sets an error" do
        line_item_in_diff_cart = carts(:two_item_cart).line_items.first
        cart.remove(item_in_diff_cart)
        expect(cart.errors[:base]).to eql [ "Unable to remove #{line_item_in_diff_cart.product.title}" ]
      end
    end
  end
end
