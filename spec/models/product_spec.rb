require 'rails_helper' 
require 'database_cleaner_helper'

RSpec.describe Product, :type => :model do
  subject { described_class.new(title: "Title minimum of 10", description: "bar", price: 10.10) }

  it { should validate_presence_of :description }
  it { should validate_length_of(:title).is_at_least(10) }
  it { should validate_presence_of :title }
  it { should validate_numericality_of(:price).is_greater_than(0) }

  it { should validate_uniqueness_of :title }
end
