require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the CartsHelper. For example:
#
# describe CartsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ApplicationHelper, :type => :helper do
  describe "#hidden_div_if" do
    it 'generates a NON-hidden div take with given attributes' do
      div = hidden_div_if(false, id: "cart") do
        "Hello"
      end
      expect(div).to eql('<div id="cart">Hello</div>')
    end

    it 'generates a HIDDEN div with given attrs' do
      div = hidden_div_if(true, id: "cart") do
        "Hello"
      end
      expect(div).to eql('<div id="cart" style="display:none">Hello</div>')
    end
  end
end
