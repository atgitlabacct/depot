require 'rails_helper'
require 'capybara_helper'

RSpec.feature "Cart management", type: feature, js: true do
  fixtures(:carts)
  fixtures(:products)
  fixtures(:line_items)

  scenario "adding and removing items to a cart", js: true do
    visit "/"

    # Lets add some items to our cart
    2.times { find(:test_id, 'coffeescript').click_button("Add to Cart") }
    find(:test_id, 'ruby-programming').click_button("Add to Cart")

    # Is the side column updating with the correct cart information
    expect(find('#side > #cart')).to have_text("2× Coffeescript")
    expect(find('#side > #cart')).to have_text("1× Ruby Programming")

    # View/Edit the Cart
    click_link "Your Cart"
    find(:test_id, 'coffeescript-quantity').set 5
    click_button 'Update cart'

    # Make sure quantities were update
    coffee_qty = find(:test_id, 'coffeescript-quantity')
    expect(coffee_qty.value).to eql '5'

    # Lets set quantity to 0 so items get removed
    coffee_qty.set 0
    find(:test_id, 'ruby-programming-quantity').set 0
    click_button 'Update cart'

    expect(page).to_not have_content "Coffeescript"
    expect(page).to_not have_content "Ruby Programming"

    #Now lets remove items via the remove all button
    visit "/"
    find(:test_id, 'ruby-programming').click_button("Add to Cart")
    click_link 'Your Cart'
    click_link 'Remove all'
    expect(page).to_not have_content "Ruby Programming"
  end
end
