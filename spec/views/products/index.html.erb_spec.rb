require 'rails_helper'

RSpec.describe "products/index", :type => :view do
  before(:each) do
    assign(:products, [
      Product.create!(
        :title => "Title is 10 or more",
        :description => "Some Description",
        :image_url => "Image Url",
        :price => "9.99"
      ),
      Product.create!(
        :title => "Poor Foo, Bar is with Baz",
        :description => "Some Description",
        :image_url => "Image Url",
        :price => "9.99"
      )
    ])
  end

  it "renders a list of products" do
    render
    assert_select "tr>td>dl>dt", :text => "Poor Foo, Bar is with Baz".to_s, :count => 1
    assert_select "tr>td>dl>dd", :text => "Some Description".to_s, :count => 2
    assert_select "tr>td>img", :count => 2
  end
end
