require 'rails_helper'

RSpec.describe "store/index.html.erb", :type => :view do
  before :each do
    assign(:products, [
      Product.create!(
        :title => "Title is 10 or more",
        :description => "Some Description",
        :image_url => "Image Url",
        :price => "9.99"
      ),
      Product.create!(
        :title => "Poor Foo, Bar is with Baz",
        :description => "Some Description",
        :image_url => "Image Url",
        :price => "9.99"
      )])
  end

  it "renders a list of products" do
    render
    assert_select "div.entry", 2
    assert_select ".entry h3", "Poor Foo, Bar is with Baz"
    assert_select ".entry h3", "Title is 10 or more"
  end

  it "adds a clickable image to add to the cart" do
    render
    assert_select 'a[data-method="post"][data-remote="true"] > img'
  end
end
