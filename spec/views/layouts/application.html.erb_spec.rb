require 'rails_helper'

RSpec.describe "layouts/application", :type => :view do
  fixtures :carts

  context "on a page that doesn't need a cart" do
    it "cart sidebar is missing" do
      render
      expect(rendered).to_not have_selector('div#cart > table[data-test-id="cart-table"]')
    end
  end

  context "on a page that should have a cart" do
    it "has a cart on the sidebar" do
      cart = carts(:one_item_cart)
      assign(:cart, cart)
      allow(controller).to receive(:current_cart)
        .and_return(cart)
      render
      expect(rendered).to have_selector('div#cart > table[data-test-id="cart-table"]')
    end
  end
end
