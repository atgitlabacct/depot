require 'rails_helper'

RSpec.describe "carts/show", :type => :view do
  fixtures(:carts)

  fixtures(:line_items)

  let(:cart) { carts(:two_item_cart) }

  it "displays multiple line items" do
    assign(:cart, cart)

    render
    expect(rendered).to match(/Generic Book 001/)
    expect(rendered).to match(/\$1\.99/)

    expect(rendered).to match(/Generic Book 002/)
    expect(rendered).to match(/\$2\.99/)
  end

  it "displays the 'Remove all' link for a line item" do
    assign(:cart, cart)
    item = cart.line_items.first

    render
    expect(rendered).to have_link('Remove all')
    expect(rendered).to have_selector('a[href="/line_items/' + item.id.to_s + '"][data-method="delete"]')
  end


  it "displays an update button to change quantities" do
    assign(:cart, cart)

    render
    expect(rendered).to have_selector('form[action="/carts/' + cart.id.to_s + '"]')
    expect(rendered).to have_selector('input[type="hidden"][name="_method"][value="put"]')
    expect(rendered).to have_selector('input[type="submit"][value="Update cart"]')
  end

end
