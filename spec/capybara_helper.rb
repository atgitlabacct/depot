Capybara.add_selector(:test_id) do
  xpath { |id| XPath.descendant[XPath.attr(:'data-test-id') == id.to_s] }
end
