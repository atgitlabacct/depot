class Product < ActiveRecord::Base
  validates :title, uniqueness: true, length: { minimum: 10,
    messag: "Yo Fool, you need at least 10 characters for title" }
  validates :description, :title, presence: true
  validates :price, numericality: { greater_than: 0 }

  has_many :line_items

  before_destroy :ensure_referenced_by_any_line_item

  def self.latest
    Product.order(:updated_at).last
  end

  private

  def ensure_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'Line Items present')
      return false
    end
  end
end
