class LineItem < ActiveRecord::Base
  belongs_to :product
  belongs_to :cart

  validates :quantity, numericality: { greater_than_or_equal_to: 0}

  delegate :title, to: :product

  def total_price
    quantity * product.price
  end
end
