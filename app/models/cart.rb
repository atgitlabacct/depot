class Cart < ActiveRecord::Base
  has_many :line_items, dependent: :destroy
  accepts_nested_attributes_for :line_items

  after_save :remove_empty_line_items

  delegate :empty?, to: :line_items

  def remove_empty_line_items
    line_items.where(quantity: 0).delete_all
  end

  def add_product(product)
    current_item = find_line_item_by_product_id(product.id)

    if current_item
      current_item.quantity += 1
    else
      current_item = line_items.build(product_id: product.id, price: product.price, quantity: 1)
    end

    current_item
  end

  def total_price
    line_items.to_a.sum { |item| item.total_price }
  end

  def remove(item)
    if line_items.include?(item)
      line_items.delete(item)
      return true 
    else 
      errors.add(:base, "Unable to remove #{item.title}")
      return false
    end
  end

  private

  def find_line_item_by_product_id(id)
    unsaved_item = line_items.select { |item| item.product_id == id }.first
    return unsaved_item unless unsaved_item.nil?
    return line_items.find_by(product_id: id)
  end
end
