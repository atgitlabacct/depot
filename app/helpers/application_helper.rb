module ApplicationHelper
  def to_test_id(val)
    val.downcase.gsub(" ", "-")
  end

  def current_cart
    if (controller.respond_to? :current_cart)
      return controller.current_cart
    end
  end

  def hidden_div_if(condition, attributes = {}, &block)
    if condition
      attributes["style"] = "display:none"
    end

    content_tag("div", attributes, &block)
  end
end
