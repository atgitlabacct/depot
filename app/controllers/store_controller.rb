class StoreController < ApplicationController
  include StoreCounter
  include CurrentCart

  before_action :set_cart
  
  def index
    if store_counter.nil?
      reset_store_counter
    else
      add_to_store_counter
    end

    @products = Product.order(:title)
  end
end
