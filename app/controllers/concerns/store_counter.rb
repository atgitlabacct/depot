module StoreCounter
  extend ActiveSupport::Concern

  def store_counter
    session[:store_counter]
  end

  private

  def reset_store_counter
    session[:store_counter] = 1
  end

  def add_to_store_counter
    session[:store_counter] += 1
  end
end
