class LineItemsController < ApplicationController
  include StoreCounter
  include CurrentCart

  before_action :set_cart, only: [:create, :destroy]
  rescue_from ActiveRecord::RecordNotFound, with: Proc.new {
    flash[:error] = "Record not found"
    redirect_to_cart
  }

  def create
    puts params
    build_line_item

    respond_to do |format|
      if save_line_item
        format.html { redirect_to store_path }
        format.js { @current_item = @line_item }
      else
        format.html { render action: 'new' }
      end
    end
  end

  def destroy
    load_line_item
    delete_line_item 
    redirect_to_cart
  end

  private
  
  def delete_line_item
    unless current_cart.remove(@line_item)
      flash[:errors] = current_cart.errors[:base]
    end
  end

  def build_line_item
    product = Product.find(params[:product_id])
    @line_item = @cart.add_product(product)
  end

  def save_line_item
    if @line_item.save
      reset_store_counter
    end
  end

  def redirect_to_cart
    redirect_to cart_path(current_cart)
  end

  def load_line_item
    @line_item ||= LineItem.find(params[:id])
  end
end
