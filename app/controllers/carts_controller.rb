class CartsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :cart_not_found

  def show
    load_cart
    invalid_cart? ? invalid_cart : render('show')
  end

  def destroy
    load_cart
    if invalid_cart?
      invalid_cart
    else
      destroy_cart
      redirect_to store_url
    end
  end

  def update
    load_cart
    update_line_items
    if @cart.save
      redirect_to cart_path(@cart)
    else
      flash[:notice] = 'Could not update your cart'
      render 'show'
    end
  end

  private

  def update_line_items
    @cart.line_items_attributes = line_items_params
  end

  def cart_not_found
    logger.error "Cart was not found with #{params[:id]}"
    redirect_to(store_url, notice: 'Invalid cart')
  end

  def invalid_cart?
    return true if @cart.id != session[:cart_id]
    return false
  end

  def invalid_cart
    logger.error "Attempt to access someone elses cart #{params[:id]}"
    redirect_to(store_url, notice: 'Invalid cart')
  end

  def load_cart
    @cart = Cart.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cart_params
    params.require(:cart)
  end

  def line_items_params
    data = cart_params.permit(line_items_attributes: [:quantity, :id])
    data[:line_items_attributes]
  end

  def destroy_cart
    @cart.destroy
    session[:cart_id] = nil
  end

end
