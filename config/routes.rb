Rails.application.routes.draw do

  get 'store/index'

  resources :products
  resources :carts, only: [:show, :destroy, :update]
  resources :line_items, only: [:create, :destroy]

  root 'store#index', as: 'store'
end
